const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  Container: {
    flex: 1,
    padding:10
  },
  topContainer:{
    flex:0.1,
    margin:10
  },
  logoContainer: {
    flex: 0.5,
    alignItems: "center",
    justifyContent: "center"
  },
  logo: {
    position: "absolute",
    width: 120,
    height: 120,
  },
  titleContainer: {
    flex: 0.5,
    alignItems: "center",
    justifyContent: "center"
  },
  titleText: {
    color: "#131313",
    fontSize: 26,
  },
  titleDesc: {
    color: "#616161",
    fontSize: 15,
    marginTop: 18,
    textAlign: "center"
  },
  formContainer: {
    flex: 1,
    backgroundColor : "#fff",
    marginBottom: 30,
  },
  input:{
    borderColor: "#d3dfef",
    borderRadius: 5
  },
};
