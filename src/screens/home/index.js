import React, { Component } from "react";
import { ImageBackground, View, StatusBar } from "react-native";
import {
  Container,
  Button,
  Text,
  Content,
  Input,
  Icon,
  Item,
  Form,
  H1,H2,H3
} from "native-base";

import styles from "./styles";

const rightArrow = require("../../../assets/Shape.png");
const launchscreenLogo = require("../../../assets/logo-mobi.png");

class Home extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <View style={styles.Container}>
          <View style={styles.topContainer}>
            <Text style={{textAlign: "right", color: "#606060"}}>Guest Login</Text>
          </View>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.titleText}>Welcome back !</Text>
            <Text style={styles.titleDesc}>Please log in with the following information in order to get all the other benefits</Text>
          </View>
          <View style={styles.formContainer}>
            <Content padder>
              <Form style={styles.input}>
                <Item regular style={{marginBottom:10,}}>
                  <Input  placeholder="School Code" />
                </Item>
                <Item regular style={{marginBottom:10,}}>
                  <Input  placeholder="User Name" />
                </Item>
                <Item regular style={{marginBottom:10,}}>
                  <Input  placeholder="Password" />
                  <Icon name="eye" />
                </Item>
              </Form>
            </Content>
            <Button block primary
                    style={{margin:10, backgroundColor: "#30cec3" }}
                    onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Text>SIGN IN</Text>
            </Button>
            <Text style={styles.titleDesc}>Forgot Password ?</Text>
          </View>
        </View>
      </Container>
    );
  }
}

export default Home;
